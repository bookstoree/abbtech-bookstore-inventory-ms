package org.abbtech.practice;

import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/m")
@RequiredArgsConstructor
@Validated
public class Controller {
    @GetMapping
    public void getOperation(){
        System.out.println("operation called");
    }


}
